<?php

require_once 'OrderLine.php';
require_once 'OrderLineDao.php';

$lines = file('data/order.txt');

$orderLines = [];
foreach ($lines as $line) {

    [$name, $price, $inStock] = explode(';', trim($line));

    $price = floatval($price); // string to float
    $inStock = $inStock === 'true'; // string to boolean

    // create new object and add it to $orderLines list
}

// print list of order line objects
foreach ($orderLines as $orderLine) {
    printf('name: %s, price: %s; in stock: %s' . PHP_EOL,
        $orderLine->productName,
        $orderLine->price,
        $orderLine->inStock ? 'true' : 'false');
}

